import { Inventory } from './inventory';

export const Inventories: Inventory[] = [
  { id: 1, name: 'Lux', manufactureDate: '2018-01-01'},
  { id: 2, name: 'Liril', manufactureDate: '2017-01-01'}

];
