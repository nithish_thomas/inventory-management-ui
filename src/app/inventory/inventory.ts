export class Inventory {
  id: number;
  name: string;
  manufactureDate: string;

  constructor(id: number, name: string, manufacturedDate: string) {
    this.id = id;
    this.name = name;
    this.manufactureDate = manufacturedDate;
  }
}
