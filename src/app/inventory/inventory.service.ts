import { Injectable } from '@angular/core';
import { Inventory } from './inventory';
import { Inventories } from './inventory-mock';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) {
  }

  getInventories(): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(environment.inventoryUrl);
  }




}
