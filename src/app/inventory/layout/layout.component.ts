import { Component, OnInit } from '@angular/core';
import { Inventories } from '../inventory-mock';
import { Inventory } from '../inventory';
import { InventoryService } from '../inventory.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  inventories: Inventory[] = [];

  constructor(private inventoryService: InventoryService) {
  }

  ngOnInit() {
    this.inventoryService.getInventories().subscribe(data => {
      console.log(data);
      this.inventories = data;
    });
}

}
