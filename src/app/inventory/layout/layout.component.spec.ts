import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutComponent } from './layout.component';
import { InventoryModule } from 'src/app/inventory/inventory.module';
import { HeaderComponent } from 'src/app/inventory/header/header.component';
import { FooterComponent } from 'src/app/inventory/footer/footer.component';
import { InventoryService } from 'src/app/inventory/inventory.service';
import { Inventory } from 'src/app/inventory/inventory';
import { of } from 'rxjs';

describe('LayoutComponent', () => {
  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;
  let getInventorySpy: any;
  let inventoryService: any;

  beforeEach(async(() => {
    const testInventory: Inventory[] = [];
    inventoryService = jasmine.createSpyObj('InventoryService', ['getInventories']);
    getInventorySpy = inventoryService.getInventories.and.returnValue(of(testInventory) );

    TestBed.configureTestingModule({
      declarations: [ LayoutComponent, HeaderComponent, FooterComponent ],
      providers:    [{ provide: InventoryService, useValue: inventoryService }],
    });
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call invetiry service getInventories() when onInit() is called', () => {
    component.ngOnInit();
    expect(inventoryService.getInventories).toHaveBeenCalled();
  });

  it('sets inventories correctly before onInit() calls', () => {
    expect(component.inventories.length).toBe(0);
  });

  it('sets inventories correctly from the service when service returns data', () => {
    const testInventory: Array<Inventory> =
    [
      new Inventory(1, 'testInventory1', '2018-01-01'),
      new Inventory(2, 'testInventory2', '2018-02-01'),
    ];
    inventoryService.getInventories.and.returnValue(of(testInventory) );
    expect(component.inventories.length).toBe(0);
    component.ngOnInit();
    expect(component.inventories.length).toBe(2);
  });


});
