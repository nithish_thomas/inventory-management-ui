import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './inventory/layout/layout.component';
import { AboutComponent } from './about/about.component';



const routes: Routes = [
  { path: 'inventories', component: LayoutComponent },
  { path: 'about', component: AboutComponent }
];


@NgModule({
  imports: [ CommonModule, RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {



 }
